<?php

/**
 * Menu callback - Admin settings form.
 */

function dojo_toolkit_admin_form() {
  $form = array();
  
  $form['build'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dojo Build'),
    '#description' => t('<p>Select the Dojo build to use below. You can use a CDN build or you can choose to make a local build containing only the packages that you need. To create a custom build, check out the <a href="/' . drupal_get_path('module', 'dojo_toolkit') . '/profiles/README.txt">README file</a>.</p>'),
    '#collapsible' => TRUE
  );

  $dojo_xdomain_urls = array(
    '//ajax.googleapis.com/ajax/libs/dojo/1.9.0/dojo/dojo.js' => 'Google CDN Dojo 1.9.0, SSL compatible',
    '//ajax.googleapis.com/ajax/libs/dojo/1.8.3/dojo/dojo.js' => 'Google CDN Dojo 1.8.3, SSL compatible',
    '//yandex.st/dojo/1.9.0/dojo/dojo.js' => 'Yandex (EU) Dojo 1.9.0',
    '//yandex.st/dojo/1.8.3/dojo/dojo.js' => 'Yandex (EU) Dojo 1.8.3',
    'http://o.aolcdn.com/dojo/1.7/dojo/dojo.xd.js' => 'AOL\'s Content Delivery Network (CDN) Dojo 1.7',
    '//ajax.googleapis.com/ajax/libs/dojo/1.7.4/dojo/dojo.js' => 'Google CDN Dojo 1.7.4, SSL compatible',
    'http://yandex.st/dojo/1.7.4/dojo/dojo.js' => 'Yandex (EU) Dojo 1.7.4',
  );
  $results = array();
  $base = base_path();
  foreach(libraries_get_libraries() as $lib_path){
    $tmp = file_scan_directory($lib_path, '/^dojo.js/');
    if ($tmp){
      foreach($tmp as $path=>$dojo_build){
        $results[$base.$path] = $dojo_build->uri;
      }
    }
  }
  $builds = array('Local Builds' =>$results , 'XDomain Builds' => $dojo_xdomain_urls);
  $desc = '<p>Select a Dojo build to load when visiting a Dojo-enabled page. This can be a build hosted by a CDN, or a downloaded copy of the Dojo source inside a libarary path. You can use drush dtk-dl to download a recent copy of Dojo Toolkit to your sites/all/libraries directory, and then it will show up here.</p>';
  if (!count($builds['Local Builds'])) {
    $desc = '<p>No local Dojo builds found. There are circumstances where you may want to host the Dojo Javascript files on this server.  You can download a Dojo build from <a href="http://download.dojotoolkit.org" target="_blank">http://download.dojotoolkit.org</a>.  It is recommended to use the <a href="http://download.dojotoolkit.org/release-1.9.0/dojo-release-1.9.0.tar.gz">Dojo 1.9.0</a> release.</p>';
  }

  $form['build']['dojo_toolkit_js'] = array(
    '#type' => 'select',
    '#title' => t('Build'),
    '#default_value' => variable_get('dojo_toolkit_js',DOJO_TOOLKIT_VERSION),
    '#options' => $builds,
    '#description' => t($desc)
  );
  
  $form['build']['dojo_toolkit_layer'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('dojo_toolkit_layer',''),
    '#title' => t('Extra Layer Files'),
    '#description' => 'When doing a build, your custom selection of code can be built into an additional layer. Specify that layer name here and it will get loaded right after the main Dojo file. It must be in the same directory.',
  );

  $form['dijit_theme'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dijit Theme'),
    '#description' => t('Enter the theme to apply to all Dijit widgets. You may enter a space delimited list of all CSS classes which are to added to the &lt;body&gt;\'s class. The theme is only applied on pages which Dojo is loaded.'),
    '#collapsible' => TRUE,
  );

  $form['dijit_theme']['dojo_toolkit_theme'] = array(
    '#type' => 'textfield',
    '#title' => t('Theme'),
    '#default_value' => variable_get('dojo_toolkit_theme', DOJO_TOOLKIT_THEME),
  );

  $form['vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility'),
    '#description' => t('You can define the pages in which this config profile should be applied.'),
    '#collapsible' => TRUE,
  );

  $options = array(
    DOJO_TOOLKIT_MODS_ONLY => t('Only show when a module specifically loads Dojo.'),
    DOJO_TOOLKIT_PAGES_EXCEPT => t('Show on every page except the listed pages.'),
    DOJO_TOOLKIT_PAGES_LISTED => t('Show on only the listed pages.'),
    DOJO_TOOLKIT_PHP_TRUE => t('Show if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).'),
  );
  $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page. If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>', '%php' => '<?php ?>'));

  $form['vis_settings']['dojo_toolkit_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Apply this config profile on specific pages'),
    '#options' => $options,
    '#default_value' => variable_get('dojo_toolkit_visibility', DOJO_TOOLKIT_MODS_ONLY ),
  );

  $form['vis_settings']['dojo_toolkit_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('dojo_toolkit_pages', ''),
    '#description' => $description,
  );
  $form['vis_settings']['dojo_toolkit_pages_requires'] = array(
    '#type' => 'textarea',
    '#title' => t('Requires'),
    '#description' => t('Specify one required module per line, and optionally a javascript alias for it to use inside the onload block.'),
    '#default_value' => variable_get('dojo_toolkit_pages_requires', 'dojo/parser|parser'),
  );
  $form['vis_settings']['dojo_toolkit_pages_onload'] = array(
    '#type' => 'textarea',
    '#title' => t('Script to run after all required libraries are loaded'),
    '#description' => t('This code gets run inside a require() block, with any required libraries assigned to their js aliases.'),
    '#default_value' => variable_get('dojo_toolkit_pages_onload', 'parser.parse();'),
  );
  $form['djconfig'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dojo Configuration'),
    '#description' => t('The following Dojo options are used to set the "<code>djConfig</code>" Javascript variable.'),
    '#collapsible' => TRUE,
  );

  $form['djconfig']['dojo_toolkit_is_debug'] = array(
    '#type' => 'radios',
    '#title' => t('Debug'),
    '#default_value' => variable_get('dojo_toolkit_is_debug',0),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('Displays Dojo debug messages.'),
  );

  $form['djconfig']['dojo_toolkit_parse_onload'] = array(
    '#type' => 'radios',
    '#title' => t('Parse Onload'),
    '#default_value' => variable_get('dojo_toolkit_parse_onload', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('Parse widgets and declarations onload - note, does not work with a CDN/Cross-domain view.'),
  );
  
  $form['djconfig']['dojo_toolkit_djconfig_extra'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra options to pass to djconfig'),
    '#default_value' => variable_get('dojo_toolkit_djconfig_extra',''),
    '#description' => 'Pass extra settings to djconfig - use for things like gfx settings',
  );

  return system_settings_form($form);
}
