<?php

/**
 * @file
 *   drush integration for dojo_toolkit.
 */

/**
 * The Dojo Toolkit download URI.
 *
 * Release URIs are in two forms: release bundle (without build tools) or
 * source bundle (with everything.)
 *
 * Release URL for 1.9.1: http://download.dojotoolkit.org/release-1.9.1/dojo-release-1.9.1.tar.gz
 * Source URL: http://download.dojotoolkit.org/release-1.9.1/dojo-release-1.9.1-src.tar.gz
 *
 * Previous releases have other version numbers in both the directory and archive.
 */
define('DOJO_TOOLKIT_DOWNLOAD_URI', 'http://download.dojotoolkit.org/release-');
define('DOJO_TOOLKIT_LATEST_VERSION','1.9.1'); // Most recent download version

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * See `drush topic docs-commands` for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function dojo_toolkit_drush_command() {
  $items = array();

  // the key in the $items array is the name of the command.
  $items['dojo_toolkit-download'] = array(
    'callback' => 'drush_dojo_toolkit_download',
    'description' => dt("Downloads the Dojo Toolkit library."),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap.
    'arguments' => array(
      'version' => dt('Version of Dojo Toolkit to download. Defaults to '.DOJO_TOOLKIT_LATEST_VERSION),
      'release' => dt('Source or Release build. Use "true" or "release" to download the stripped down release version. Use "source" or false or omit to download the full source, with build tools (default) '),
      'path' => dt('Optional. A path where to install the Dojo Toolkit library. If omitted Drush will use sites/all/libraries.'),
    ),
    'aliases' => array('dtk-dl'),
  );
  
  $items['dojo_toolkit-build'] = array(
    'callback' => 'drush_dojo_toolkit_build',
    'description' => dt('Create a dojo build. Not yet implemented.'),
    'aliases' => array('dtk-build'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function dojo_toolkit_drush_help($section) {
  switch ($section) {
    case 'drush:dojo_toolkit-download':
      return dt("Downloads the Dojo Toolkit from dojotoolkit.org, default location is sites/all/libraries.");
    case 'drush:dojo_toolkit-build':
      return dt('Uses node.js or Rhino to create a custom Dojo build. Not yet implemented.');
  }
}

/**
 * Command to download the Dojo Toolkit library.
 */
function drush_dojo_toolkit_download($version = DOJO_TOOLKIT_LATEST_VERSION, $release = false, $path = 'sites/all/libraries') {
  if (!drush_shell_exec('type tar')) {
    return drush_set_error(dt('Missing dependency: tar. Install it before using this command.'));
  }
  
  $suffix = '-src';
  if (!empty($release) && ($release === true || $release == 'true' || strcasecmp($release,'release') == 0)) {
    $suffix = '';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);
  
  $url = DOJO_TOOLKIT_DOWNLOAD_URI.$version. '/dojo-release-' .$version.$suffix;

  $filename = basename($url . '.tar.gz');
  $dirname = basename($url);

  // Remove any existing Dojo Toolkit directory
  if (is_dir($dirname)) {
    drush_log(dt('An existing Dojo Toolkit library was overwritten at @path', array('@path' => $path)), 'notice');
  }
  // Remove any existing Dojo Toolkit archive
  if (is_file($filename)) {
    drush_op('unlink', $filename);
  }

  drush_log(dt('Downloading @url to @path', array('@url'=>$url.'.tar.gz', '@path'=>$path)),'debug');
  // Download the zip archive
  if (!drush_shell_exec('wget ' . $url.'.tar.gz')) {
    drush_shell_exec('curl -O ' . $url . '.tar.gz');
  }

  if (is_file($filename)) {
    // Decompress the archive
    drush_shell_exec('tar -xvzf ' . $filename);
    // Remove the archive
    drush_op('unlink', $filename);
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);

  if (is_dir($path . '/' . $dirname)) {
    drush_log(dt('Dojo Toolkit @version has been downloaded to @path', array('@version'=> $version, '@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to download Dojo Toolkit @version to @path', array('@version'=>$version,'@path' => $path)), 'error');
  }
}
