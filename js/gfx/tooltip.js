define(['..','dojo/_base/lang','dojox/gfx/shape','dojox/gfx/matrix','dojo/NodeList-traverse', 'dijit/Tooltip'],
	function(dojo_toolkit, dojo, shape, matrix, traverse, tt){
	  if (!dojo.isObject(dojo_toolkit.gfx)) {
			dojo_toolkit.gfx = {};
		}
		
				
		function _normalizeArround(gfxObject){
			var bbox = _getRealBBox(gfxObject);
			var rawNode = gfxObject.getNode();
			//get the real screen coords for gfx object
			var realMatrix = gfxObject._getRealMatrix() || {xx:1,xy:0,yx:0,yy:1,dx:0,dy:0};
			var point = dojox.gfx.matrix.multiplyPoint(realMatrix, bbox.x, bbox.y);
			var gfxDomContainer = _getGfxContainer(gfxObject);
			gfxObject.x = dojo.coords(gfxDomContainer,true).x + point.x,
			gfxObject.y = dojo.coords(gfxDomContainer,true).y + point.y,
			gfxObject.width = bbox.width * realMatrix.xx,
			gfxObject.height = bbox.height * realMatrix.yy
			return gfxObject;
		}
	
		function _getGfxContainer(gfxObject){
			return (new dojo.NodeList(gfxObject.rawNode)).parents("div")[0];
		}
	
		function _getRealBBox(gfxObject){
			var bboxObject = gfxObject.getBoundingBox();
			if(!bboxObject){ //the gfx object is group
				var shapes = gfxObject.children;
				if (!shapes) {
					return null;
				}
				var bboxObject = dojo.clone(_getRealBBox(shapes[0]));
				dojo.forEach(shapes,function(item){
					var nextBBox = _getRealBBox(item);
					if (nextBBox) {
						bboxObject.x = Math.min(bboxObject.x, nextBBox.x);
						bboxObject.y = Math.min(bboxObject.y, nextBBox.y);
						bboxObject.endX = Math.max(bboxObject.x + bboxObject.width, nextBBox.x + nextBBox.width);
						bboxObject.endY = Math.max(bboxObject.y + bboxObject.height, nextBBox.y + nextBBox.height);
					}
				})
				bboxObject.width = bboxObject.endX - bboxObject.x;
				bboxObject.height = bboxObject.endY - bboxObject.y;
			}
			return bboxObject;
		}
		
		var dgtt = dojo.extend(shape.Shape, {
				
			showTooltip: function(/*String*/ innerHTML, /*String[]?*/ position) {
				// summary:
				//		Display tooltip around this shape w/specified contents in specified position.
				//		See description of dijit.Tooltip.defaultPosition for details on position parameter.
				//		If position is not specified then dijit.Tooltip.defaultPosition is used.
				this._tooltipNode = _normalizeArround(this);
				return dijit.showTooltip(innerHTML, this._tooltipNode, position);
			},
			
			hideTooltip: function() {
				// summary:
				//		Hide the tooltip around this shape
				var arroundNode = this._tooltipNode;
				if (arroundNode) {
					delete this._tooltipNode;
					return dijit.hideTooltip(arroundNode);
				}
			}
		});
		dojo_toolkit.gfx.tooltip = dgtt;
		return dgtt;
});
